<!doctype html>
<?php include 'config.php'; ?>
<html>
	<head>
		<meta charset="utf-8">
		<title>xdd</title>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
		<link rel="stylesheet" href="css/reset.css">
		<!--<link rel="stylesheet" href="style.css">-->
		<link rel="stylesheet" href="css/main.css">
	</head>
	
	<body>
		<header>
			<h1>
				Quiz app
			</h1>
			<nav>
 				<?php
				
				if($_SESSION['logged']==true) {
					echo '<div>zalogowany jako(id): '.$_SESSION['id'].'</div>
					<div><a href="logout.php">wyloguj</a></div>';
				}
				else {
					echo '<div id="register_link">rejestracja</div>
					<div id="login_link">Zaloguj</div>';	
				}
				 ?>
			</nav>
			<div id="register_box">
				<form action="register.php" method="POST">
					<input type="text" id="name" name="name">
					<input type="text" id="email" name="email">
					<input type="password" id="pass1" name="pass1">
					<input type="password" id="pass2" name="pass2">
					<div class="button" id="register">wyślij</div>
				</form>
			</div>

			<div id="login_box">
				<form action="login.php" method="POST">
					<input type="text" id="user" name="user">
					<input type="password" id="password" name="password">
					<div class="button" id="login">wyślij</div>
				</form>
			</div>
		</header>
		
		<main>
			<div id="res" style="display:none;">|</div>

			<?php 
			$sql = $mysqli->query("SELECT * FROM `questions` ORDER BY RAND()");
			while ($question = $sql->fetch_assoc()) { 
				echo '<ul class="question" id="q'.$question['id'].'">
					<li class="qcontent">'.$question['content'].'</li>';
					$sql2 = $mysqli->query("SELECT * FROM `answers` where `id_question` = '{$question['id']}' ORDER BY RAND() ");
					while ($answer = $sql2->fetch_assoc()) {
						echo '<li class="answer" id="a'.$answer['id'].'">'. $answer['content'] .'</li>';
					}
				echo '</ul><input type="hidden" value="" class="reply" id="reply'.$question['id'].'">';
			} ?>
			
			<div class="button" id="send">wyślij</div>
		</main>
		<script src="js/index.js"></script>
	</body>
</html>
<?php $mysqli->close(); ?>