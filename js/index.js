// ta funkcja zmienia odpowiedź wybraną przez usera
$(document).on('click','.answer', function(){

	if ( $(this).hasClass("disabled") == false ) {
		var id = $(this).parent().attr('id').substring(1); 									// bierze id pytania
		$("#q"+id+" .answer").removeClass('selected');											// usuwa klase selected z innych odpowiedzi
		$(this).addClass('selected');																				// i daje ją tylko tej jednej
		$("#a"+id+"[type=checkbox]").prop("disabled", true);
		$("#reply"+id).val($(this).attr('id').substring(1));
	}
});

$(document).on('click','.answer.selected', function(){
	if ( $(this).hasClass("disabled") == false ) {
		var id = $(this).parent().attr('id').substring(1);									// bierze id pytania
		$(this).removeClass("selected");
		$("#a"+id+"[type=checkbox]").prop("disabled", false);
		$("#reply"+id).val("");
	}
});

$("#send").click(function() {
	$("#res").css("display", "block");
	$("html, body").animate({ scrollTop: 0 }, 600);												// przewija na góre strony v2
	$(".answer").addClass("disabled");																		// użyte do blokowania zaznaczania
	var data = "";
	var questions = $(".question").size();																// sprawdza ile pytań
	var i = 0;
	while ( i < questions ){
		i++;
		var id = $(".reply:nth-of-type("+i+")").attr('id'); 								//bierze po kolei każde pytanie i sprawdza ich id w bazie
		var reply = $("#"+id).val(); 																				//potem sprawdza jakiej odpowiedzi ktoś udzielił
		var string = id+"="+reply+"&"; 																			// robi stringa
		data += string; 																										// i dodaje go do datastringa do ajaxa
	}

	var dataString = 'action=check&'+data;
	$.ajax({
		method: "POST",
		url: "api.php",
		data: dataString,
		success: function(data){
			$("#res").html(data);
		},
	});
});

$("#login").click(function() {
	var dataString = "action=login&user=" + $("#user").val() + "&password=" + $("#password").val();
	$.ajax({
		method: "POST",
		url: "api.php",
		data: dataString,
		success: function(data){
			$("#res").html(data).css("display", "block");
		},
	});
});

$("#register").click(function() {
	var dataString = "action=register&name="+ $("#name").val() + "&email=" + $("#email").val() + "&pass1=" + $("#pass1").val() + "&pass2=" + $("#pass2").val();
	console.log(dataString);

	$.ajax({
		method: "POST",
		url: "api.php",
		data: dataString,
		success: function(data){
			$("#res").html(data).css("display", "block");
		},
	});
});

$("#register_link").click(function() {
	if ($("#register_box").css("display") == "none") {
		$("#register_box").css("display", "block");
		$("#login_box").css("display", "none");
	} else {
		$("#register_box").css("display", "none");
	}
});

$("#login_link").click(function() {
	if ($("#login_box").css("display") == "none") {
		$("#login_box").css("display", "block");
		$("#register_box").css("display", "none");
	} else {
		$("#login_box").css("display", "none");
	}
});

// TO TRZEBA FIXNĄĆ

// $("body").not("#register_box, #login_box, #register_link, #login_link").click(function() {
// 	if($("#login_box").css("display") == "block" || $("#register_box").css("display") == "block") {
// 		$("#login_box").css("display", "none");
// 		$("#register_box").css("display", "none");
// 	}
// });