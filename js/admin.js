$("input[name=correct]").click(function() {
	$("#correct").val($(this).attr('id').substring(7));
});
// ----------------------------------------------------- ODŚWIEŻANIE DIVÓW Z PYTANIAMI I QUIZAMI
function refresh(div,type) {
	var dataString = 'action=refresh&type='+type;
	$.ajax({
		method: "POST",
		url: "api.php",
		data: dataString,
		success: function(data){
			$(div).html(data);
		},
	});
};

// ----------------------------------------------------- DODAWANIE PYTANIA
$("#question").click(function() {
	var dataString = "action=question&question="+$("input[name=question]").val()+"&answer1="+$("input[name=answer1]").val()+"&answer2="+$("input[name=answer2]").val()+"&answer3="+$("input[name=answer3]").val()+"&answer4="+$("input[name=answer4]").val()+"&correct_answer="+$("input[name=correct_answer]").val();
	$.ajax({
		method: "POST",
		url: "api.php",
		data: dataString,
		success: function(data){
			$("#res").html(data);
			refresh('#questions-container','questions');		
			if (data.includes("Nie") != true) {
				$("#question_form input").val("");
				$("#question_form input[type=radio]").val(0);
			}
		},
	});

	
});

// ----------------------------------------------------- DODAWANIE QUIZU
$("#quiz").click(function() {
	var dataString = "action=quiz&quiz="+$("input[name=quiz]").val();
	$.ajax({
		method: "POST",
		url: "api.php",
		data: dataString,
		success: function(data){
			$("#res").html(data);
			refresh('#quizzes-container','quizzes');
		},
	});
});

// ----------------------------------------------------- USUWANIE PYTANIA Z QUIZU
var i = 0;
var quiz_id;
$(document).on('click','.tag i',function(){
	var type = $(this).attr('id');
	if (type.includes('rmfromquiz') == true) {
		var action = type.substring(0, 10);
		var id = type.substring(10).split("_");
		var dataString = "action=rmfromquiz&quiz="+id[0]+"&question="+id[1];
		$.ajax({
			method: "POST",
			url: "api.php",
			data: dataString,
			success: function(data){
				$("#res").html(data);
				refresh('#quizzes-container','quizzes');
				refresh('#questions-container','questions');
				refresh('#addquestion', 'leftquestions'+'_'+quiz_id);
			},
		});
	}
});

// ----------------------------------------------------- USUWANIE QUIZU
$(document).on('click','.quizname i',function(e){
	var type = $(this).attr('id');
	if (type.includes('quizrm') == true) {
		var id = type.substring(6);
		var dataString = "action=quizrm&quiz="+id;
		$.ajax({
			method: "POST",
			url: "api.php",
			data: dataString,
			success: function(data){
				$("#res").html(data);
				refresh('#quizzes-container','quizzes');
				refresh('#questions-container','questions');
			},
		});
	} 
// ----------------------------------------------------- DODWANIE PYTANIA DO QUIZU
	else if (type.includes('addtoquiz') == true) {
		if (i == 0) {
			i = 1;
			var x = e.pageX + 'px';
			var y = e.pageY + 'px';
			$("#addquestion").css({"top" : y, "left" : x});
			$("#addquestion").fadeIn();
			quiz_id = $(this).attr('id').substring(9);
			refresh('#addquestion', 'leftquestions'+'_'+quiz_id);
		}
		else {
			i = 0;
			$("#addquestion").fadeOut();
		}
	}
});

// ----------------------------------------------------- USUWANIE PYTANIA
$(document).on('click','#questions-container .tag i',function(){
	var id = $(this).attr('id').substring(10);
	var dataString = "action=questionrm&question="+id;
	$.ajax({
		method: "POST",
		url: "api.php",
		data: dataString,
		success: function(data){
			$("#res").html(data);
			refresh('#quizzes-container','quizzes');
			refresh('#questions-container','questions');
		},
	});
});

// ----------------------------------------------------- DODAWANIE PYTANIA (WYBIERANIE Z POJAWIAJĄCEGO SIĘ DIVA)
$(document).on('click','#addquestion .tag',function(){
	i = 0;
	var question_id = $(this).attr('id').substring(11);
	var dataString = "action=addtoquiz&quiz="+quiz_id+"&question="+question_id;
	$.ajax({
		method: "POST",
		url: "api.php",
		data: dataString,
		success: function(data){
			$("#res").html(data);
			refresh('#quizzes-container','quizzes');
			refresh('#questions-container','questions');
			$("#addquestion").fadeOut();
		},
	});
});

$(document).on('click','.tab',function(){
	var id = $(this).attr('id').substring(4);
	console.log(id);
	$(".container").css("display", "none");
	$("#"+id+".container").css("display", "block");
});
refresh('#questions-container', 'questions');
refresh('#quizzes-container', 'quizzes');