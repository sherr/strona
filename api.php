<?php
include 'config.php';
echo '<script>$("#res").removeClass("error");</script>';
switch ($_POST['action']) {

	case "check":
		$good = 0;
		$bad = 0;
		$correct_answers = "";
		unset($_POST['action']);
		foreach ($_POST as $name => $val) {
			$questionid = str_replace("reply","",$name);
			$sql = $mysqli->query("SELECT * FROM `questions` WHERE id=$questionid");
			$question = $sql->fetch_assoc();
			if ($question['answer'] == $val) {
				$good++;
			} else {
				$bad++;
			}
			$correct_answers = $correct_answers ." #a" . $question['answer'] . ",";
		}
		$correct_answers = substr($correct_answers, 0, -1);
		echo 'Odpowiedziałeś dobrze na '.$good.' pytań, a źle na '.$bad.'!';
		echo '<script>$(".answer").addClass("wrong"); $("'.$correct_answers.'").removeClass("wrong").addClass("correct");</script>';
		break;

// -------------------------------------------------------- DODAWANIE PYTANIA
	case "question":
		if ( empty($_POST['question']) OR empty($_POST['answer1']) OR empty($_POST['answer2']) OR empty($_POST['answer3']) OR empty($_POST['answer4']) OR empty($_POST['correct_answer']) ) {
					echo 'Nie wypełniłeś wszystkich pól.<script>$("#res").addClass("error");</script>';
		} else {
			// clear zapobiega sql injection
			$answer[1] = clear($_POST['answer1']);
			$answer[2] = clear($_POST['answer2']);
			$answer[3] = clear($_POST['answer3']);
			$answer[4] = clear($_POST['answer4']);
			$question = clear($_POST['question']);
			$correct_answer = clear($_POST['correct_answer']);
						
			$mysqli->query("INSERT INTO `questions` (content) VALUES ('$question')");
			$id = $mysqli->insert_id;
						
			$i = 1;
			while ($i<=4) {
				$answer_content = $answer[$i];
				$mysqli->query("INSERT INTO `answers` (content, id_question) VALUES ('$answer_content','$id')");
	
				if($i==$correct_answer) {
					$correct_id = $mysqli->insert_id;
					$mysqli->query("UPDATE `questions` SET answer=$correct_id WHERE id='$id'");
				}
				$i++;
			}
			echo 'Dodano pytanie.';
		}
		break;


// -------------------------------------------------------- USUWANIE PYTANIA
	case "questionrm":
		$question_id = $_POST['question'];
		$mysqli->query("DELETE FROM `questions` WHERE `id`='$question_id'");
		$mysqli->query("DELETE FROM `quiz_questions` WHERE `question_id`='$question_id'");
		echo 'Usunięto pytanie.';
		break;


// -------------------------------------------------------- DODAWANIE QUIZU
	case "quiz":
		if ( empty($_POST['quiz']) ) {
				echo 'Nie wpisałeś nazwy quizu.<script>$("#res").addClass("error");</script>';
		} else {
			// clear zapobiega sql injection
			$quiz = clear($_POST['quiz']);
			
			$mysqli->query("INSERT INTO `quizzes` (name) VALUES ('$quiz')");
				echo 'Dodano quiz.';
		}
		break;


// -------------------------------------------------------- USUWANIE QUIZU
	case "quizrm":
		$quiz_id = $_POST['quiz'];
		$mysqli->query("DELETE FROM `quizzes` WHERE id=$quiz_id");
		$mysqli->query("DELETE FROM `quiz_questions` WHERE quiz_id=$quiz_id");
		echo 'Usunięto quiz.';
		break;

// -------------------------------------------------------- DODAWANIE PYTANIA DO QUIZU
	case "addtoquiz":
		$quiz_id = $_POST['quiz'];
		$question_id = $_POST['question'];
		$sql = $mysqli->query("INSERT INTO `quiz_questions` (question_id,quiz_id) VALUES ($question_id,$quiz_id)");
		echo 'Dodano pytanie do quizu.';
		break;

// -------------------------------------------------------- USUWANIE PYTANIA Z QUIZU
	case "rmfromquiz":
		$quiz_id = $_POST['quiz'];
		$question_id = $_POST['question'];
		$mysqli->query("DELETE FROM `quiz_questions` WHERE quiz_id=$quiz_id AND question_id=$question_id");
		echo 'Usunięto pytanie z quizu.';
		break;

// -------------------------------------------------------- UDOSTĘPNIANIE QUIZU USEROM
	case "userquiz":

		break;

// -------------------------------------------------------- ODŚWIEŻANIE LIST
	case "refresh":
		if ($_POST['type']=='questions') {
			$sql = $mysqli->query("SELECT * FROM `questions`");
			while ($question = $sql->fetch_assoc()) {
				$id = $question['id'];
				$sql2 = $mysqli->query("SELECT * FROM quiz_questions LEFT JOIN quizzes ON quiz_questions.quiz_id=quizzes.id WHERE quiz_questions.question_id=$id");
				echo '<div class="tag" id="questiontag'.$question['id'].'">'.$question['content'].' <i class="fa fa-trash" id="questionrm'.$question['id'].'"></i></div>';
				while($quiz = $sql2->fetch_assoc()) {
					echo '';
				}
			}
		}


		else if ($_POST['type']=='quizzes') {
			$sql = $mysqli->query("SELECT * FROM `quizzes`");
			while ($quiz = $sql->fetch_assoc()) {
				$id = $quiz['id'];
				$sql2 = $mysqli->query("SELECT * FROM quiz_questions LEFT JOIN questions ON quiz_questions.question_id=questions.id WHERE quiz_questions.quiz_id=$id");
				echo '<div class="quizname">'.$quiz['name'].' <i class="fa fa-trash" id="quizrm'.$quiz['id'].'"></i> <i class="fa fa-plus" id="addtoquiz'.$quiz['id'].'"></i> <i class="fa fa-share" id="sharequiz'.$quiz['id'].'"></i></div>';
				while($question=$sql2->fetch_assoc()) {
					echo '<div class="tag">'.$question['content'].' <i class="fa fa-times" id="rmfromquiz'.$quiz['id'].'_'.$question['id'].'"></i></div>';
				}
				echo '<hr/>';
			} 
		}


		else {
			$id = explode("_", $_POST['type']);
			$quiz_id = $id[1];
			$sql = $mysqli->query("SELECT id, content FROM `questions` WHERE id NOT IN ( SELECT question_id FROM `quiz_questions` WHERE quiz_id = $quiz_id )");


			if ($sql->num_rows == 0 ) {
				echo '<div class="tag">Nie ma pytań.</div>';
			}
			else {
				while ($question = $sql->fetch_assoc()) {
					echo '<div class="tag" id="questiontag'.$question['id'].'">'.$question['content'].'</div>';
				}
			}
		}
		break;

// -------------------------------------------------------- LOGOWANIE
	case "login":
		if($_SESSION['logged']==false) {
			// jeśli zostanie naciśnięty przycisk "Zaloguj"
			if(isset($_POST['user'])) {
				$_POST['user'] = clear($_POST['user']);
				$_POST['password'] = clear($_POST['password']);
				$result = $mysqli->query("SELECT * FROM `users` WHERE `name` = '{$_POST['user']}'  LIMIT 1");
				$row = $result->fetch_assoc();
				if (sha1($_POST['password'])==$row['password']) {
					$_SESSION['logged']=true;
					$_SESSION['id']=$row['id'];
					echo '<script>location.reload();</script>';
				
				} else {
					echo 'Złe dane logowania.';		
				}
			}
		}
		break;

// -------------------------------------------------------- REJESTRACJA
	case "register":
		if($_SESSION['logged']==false) {		

			$_POST['name'] = clear($_POST['name']);
			$_POST['password'] = clear($_POST['pass1']);
			$_POST['password2'] = clear($_POST['pass2']);
			$_POST['email'] = clear($_POST['email']);


			// sprawdzamy czy wszystkie pola zostały wypełnione
			if(empty($_POST['name']) || empty($_POST['password']) || empty($_POST['password2']) || empty($_POST['email'])) {
				echo 'Musisz wypełnić wszystkie wymagane pola!';
				
			// sprawdzamy czy podane dwa hasła są takie same
			} elseif($_POST['password'] != $_POST['password2']) {
				echo 'Hasła muszą być identyczne!';
				
			// sprawdzamy poprawność emaila
			} else if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) == false) {
				echo 'Email jest niepoprawny!"';
		
		   // i czy hasło jest dłuższe niż 8 liter
			} elseif (strlen($_POST['password'])<8) {
				echo 'hasło musi zawierać od 8 do 20 znaków!';
			
			// i czy login jest dłuższy niż 4 litery
			} elseif (strlen($_POST['name'])<4 OR strlen($_POST['name'])>15) {
				echo 'Login musi zawierać od 4 do 15 znaków!';
			 
			} elseif (!preg_match('/^[a-z]*[0-9]*[-_]*$/', $_POST['name'])) {
				echo 'Login zawiera niedozwolone znaki!';
			}
			 else {
				// sprawdzamy czy są jacyś uzytkownicy z takim loginem lub adresem email
				$sql="SELECT * FROM `users` WHERE `name` = '{$_POST['name']}' OR `email` = '{$_POST['email']}'";
				$result = $mysqli->query($sql);
				$row = $result->fetch_row();
				if($row > 0) {
					echo 'Użytkownik z tym adresem e-mail lub loginem już istnieje!';
				} 
				else {			
					// jeśli nie istnieje to kodujemy haslo...
					$pass = sha1($_POST['password']);
					$name = $_POST['name'];
					$email = $_POST['email'];
					$mysqli->query("INSERT INTO `users` (name,password,email) VALUES ($name, $pass, $email)");
					echo 'Konto zostało stworzone!';
				}
			}
			
		}
		else {
			header("Location:index.php");
		}
		break;
}
?>

