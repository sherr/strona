<!doctype html>
<?php include 'config.php' ?>
<html>
	<head>
		<meta charset="utf-8">
		<title>xdd</title>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
		<link rel="stylesheet" href="css/reset.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
	</head>
	
	<body>
		<header>
			<h1>
				NAJLEPSZA STRONA
			</h1>


			<nav>
 				<?php			
					if($_SESSION['logged']==true) {
						echo 'zalogowany jako(id): '.$_SESSION['logged'].' | <a href="logout.php">wyloguj</a>';
					}
					else {
						echo '<a href="register.php">rejestracja</a> | <a href="login.php">logowanie</a>';	
					}
				?>
			</nav>

		</header>
		
		<main>
			<div id="res">Witaj w panelu administracyjnym.</div>
			<nav id="admin_menu">
				<ul>
					<li id="show1" class="tab">Dodawanie pytania</li>
					<li id="show2" class="tab">Dodawanie quizu</li>
					<li id="show3" class="tab">Wszystkie quizy</li>
					<li id="show4" class="tab">Wszystkie pytania</li>
				</ul>
			</nav>
			<div class="container" id="1">
				<h2>Dodawanie pytania</h2>
				<form id="question_form" method="POST" action="admin.php">
						Treść pytania: <input type="text" name="question"><br/>
						Odpowiedź 1: <input type="text" name="answer1"><input type="radio" name="correct" id="correct1"><br/>
						Odpowiedź 2: <input type="text" name="answer2"><input type="radio" name="correct" id="correct2"><br/>
						Odpowiedź 3: <input type="text" name="answer3"><input type="radio" name="correct" id="correct3"><br/>
						Odpowiedź 4: <input type="text" name="answer4"><input type="radio" name="correct" id="correct4"><br/>
						(zaznacz poprawną)
						<input type="hidden" name="correct_answer" value="" id="correct">
						<div id="question" class="button">DODAJ PYTANIE</div>
				</form>
			</div>

			<div class="container" id="2">
				<h2>Dodwanie quizu</h2>
				<form method="POST" action="admin.php">
					Nazwa quizu: <input type="text" name="quiz" ><br/>
					<div id="quiz" class="button">DODAJ QUIZ</div>
				</form>
			</div>
			
			<div class="container" id="3">
				<h2>Wszystkie quizy</h2>
				<div id="quizzes-container"></div>
				<div id="addquestion" style="display: none"></div>
			</div>
			
			<div class="container" id="4">
				<h2>Wszystkie pytania</h2>
				<div id="questions-container"></div>
			</div>
		</main>

		<script src="js/admin.js"></script>
	</body>
</html>
<?php $mysqli->close(); ?>